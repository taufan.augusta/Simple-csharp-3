
namespace ConsoleApp3
{
    public abstract class Shape
    {
        protected float radius;
        protected float height;

        protected Shape(float radius, float height)
        {
            this.radius = radius;
            this.height = height;
        }

        public abstract float SurfaceArea();
    }
}