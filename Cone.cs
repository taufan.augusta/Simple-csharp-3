using System;

namespace ConsoleApp3
{
    public class Cone : Shape, ICalculateable
    {
        private float slant;

        public Cone(float radius, float height)
            :base(radius, height)
        { 
            slant = (float) Math.Sqrt(Math.Pow(radius, 2) + Math.Pow(height, 2));
        }

        public override float SurfaceArea()
            => MathF.PI * radius * (radius + slant);
            // => MathF.PI * radius * slant + MathF.PI * radius * slant;

        public void Result()
        {
            float surfaceArea = (float) SurfaceArea();
            System.Console.WriteLine($"Surface Area of Cone is {surfaceArea} cm^2");
        }
    }
}

//  phi r2 + phi r s
// phi r (r+s)